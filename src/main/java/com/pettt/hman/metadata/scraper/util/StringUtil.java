package com.pettt.hman.metadata.scraper.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

@Slf4j
public class StringUtil {
    private static Random r = new Random();

    public static String readBody(File data) throws IOException {
        StringBuilder body = new StringBuilder();
        try (Stream<String> stream = Files.lines(Paths.get(data.getAbsolutePath()))) {
            stream.forEachOrdered(line -> body.append(line).append(System.getProperty("line.separator")));
        }
        return body.toString();
    }

    public static String getArgument(String[] args, String param){
        for (String argument: args) {
            if(argument.startsWith(param)){
                return argument.substring(param.length());
            }
        }

        throw new RuntimeException("Argument "+param+ " not found!");
    }

    public static List<String> getAllArguments(String[] args, String param){
        List<String> arg = new ArrayList<>();

        for (String argument: args) {
            if(argument.startsWith(param)){
                arg.add(argument.substring(param.length()));
            }
        }

        return arg;
    }

    public static String replaceArguments(String base, Map<String, String> args){
        String replaced = base;

        for (String arg : args.keySet()) {
            Pattern argsPattern = Pattern.compile("\\{"+arg+"}");
            Matcher argsMatcher = argsPattern.matcher(replaced);
            replaced = argsMatcher.replaceAll(args.get(arg));
        }

        return replaced;
    }

    public static Integer getRandomMinute(int upperBoundInclusive) {
        return r.nextInt(upperBoundInclusive) + 1;
    }

    public static List<NameValuePair> convertToPairs(String scrapeForm) {
        List<NameValuePair> pairList = new ArrayList<>();

        for (String pair : scrapeForm.split(System.getProperty("line.separator"))) {
            String name = pair.split("=",-1)[0];
            String value = pair.split("=",-1)[1];
            pairList.add(new BasicNameValuePair(name,value));
        }

        return pairList;
    }
}
