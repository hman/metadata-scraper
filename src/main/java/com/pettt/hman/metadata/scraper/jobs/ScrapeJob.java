package com.pettt.hman.metadata.scraper.jobs;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pettt.hman.metadata.scraper.MetadataScraperApplication;
import com.pettt.hman.metadata.scraper.async.AsyncJobCalls;
import com.pettt.hman.metadata.scraper.util.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.NameValuePair;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SuppressWarnings({"unchecked", "WeakerAccess", "Duplicates"}) // JSON Pojo class without getters and setters
@Slf4j
public class ScrapeJob {
    public ScrapeJob(AsyncJobCalls jobCalls, MetadataScraperApplication metadataScraperApplication, JobPojo job) {
        this.job = job;
        this.jobCalls = jobCalls;
        this.jobs = metadataScraperApplication;
    }

    private JobPojo job;
    private AsyncJobCalls jobCalls;
    private MetadataScraperApplication jobs;
    private ObjectMapper mapper = new ObjectMapper();

    public void start() throws IOException {
        forwardedStart(null);
    }

    private void forwardedStart(String path) {
        if(job.depends != -1 && path == null) {
            log.info("[{}@{}] Job depends on input from job {}",
                    job.name,job.id,job.depends);
            return;
        }

        try {
            switch (((String) job.input.get("type"))) {
                case "HTML":
                    startHtmlJobWithInput(path);
                    return;
                case "INPUT":
                    startHtmlJobWithInput(path);
                    return;
                case "IMAGE":
                    startHtmlJobWithInput(path);
                    return;
                case "API":
                    startApiJobWithInput(path);
                    return;
                case "API_FORM":
                    startApiFormJobWithInput(path);
                    return;
                case "EXTRACT":
                    startExtractJobWithInput(path);
                    return;
            }
        } catch (Exception e){
            log.error("[{}] Job failed: {}",job.id, e.getMessage());
            return;
        }

        throw new RuntimeException("Unknown forwarded start job "+ job.input.get("type")+ " id:"+job.id);
    }

    private void startExtractJobWithInput(String path) throws IOException {
        if(path == null) throw new RuntimeException("Input has to be specified for a extract job!");

        // read input file into string
        File inputFile = new File(path);
        String body = StringUtil.readBody(inputFile);

        //check if already moved
        String format = (String) job.output.get("format");
        String savePath = job.output.get("path") + inputFile.getName();
        savePath = savePath.replaceAll("\\.[0-9A-Za-z]*?$","."+format);

        File outputFile = new File(savePath);
        if (outputFile.exists() && !job.force) {
            log.debug("[{}] Skipping extracting, {} already exists", job.id, savePath);
            if(job.forward != -1)
                jobs.getJob(job.forward).forwardedStart(savePath);

            return;
        }

        String resultBody;

        // parse files and create regex groups
        Map<String, Object> parser = (Map<String, Object>) job.input.get("parse");
        String regex = (String) parser.get("regex");
        Map<String,Integer> groups = (Map<String, Integer>) parser.get("groups");

        //match regex with input file exactly once
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(body);
        if(!m.find())
            throw new IOException("Extract regex not found in file "+path); //bad input

        //name -> match Map
        Map<String, String> groupedRegex = new HashMap<>();

        //parse specified groups into map (name -> match) for later retrieval
        groups.forEach((name, group) -> groupedRegex.put(name, m.group(group)));

        //generate body
        { // find {} groups
            resultBody = groupedRegex.get("content");

            //apply operations
            Integer from = (Integer) job.getOutput("substring-from");
            Integer to = (Integer) job.getOutput("substring-to");

            resultBody = resultBody.substring(from, resultBody.length() - to);

            //remove regex

            List<String> toRemove = (List<String>) job.getOutput("remove");

            //save and forward output, if specified
            for (String removeRegex : toRemove) {
                resultBody = resultBody.replaceAll(removeRegex,"");
            }

            File saveOutput = new File(savePath);
            saveOutput.getParentFile().mkdirs();

            saveOutput.delete();

            if(format.equalsIgnoreCase("json")){//special case for json, use pretty printer
                Object json = mapper.readValue(resultBody, Object.class);
                resultBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
            }


            try( PrintWriter out = new PrintWriter(saveOutput) ) {
                out.println( resultBody );
            }

            log.info("Finished extracting {}", savePath);
            if(job.forward != -1) {
                jobs.getJob(job.forward).forwardedStart(savePath);
            }
        }
    }

    private void startApiJobWithInput(String path) throws IOException {
        //api url
        String apiUrl = (String) job.getInput("api-url");
        //path to api request body
        String apiBodyPath = (String) job.getInput("api-body");
        File apiBodyFile = new File(apiBodyPath);
        String body = StringUtil.readBody(apiBodyFile);

        //parse input file
        Map<String, Object> parser = (Map<String, Object>) job.getInput("parse");
        String regex = (String) parser.get("regex");
        Map<String,Integer> groups = (Map<String, Integer>) parser.get("groups");

        File inputFile = new File(path);
        String inputBody = StringUtil.readBody(inputFile);

        //match regex of input
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(inputBody);
        while (m.find()) { //create api request for each match
            //name -> match Map
            Map<String, String> groupedRegex = new HashMap<>();

            //parse specified groups into map (name -> match) for later retrieval
            groups.forEach((name, group) -> groupedRegex.put(name, m.group(group)));

            {//generate request body
                String requestBody = StringUtil.replaceArguments(body, groupedRegex);

                forwardApiOutput(apiUrl, requestBody, groupedRegex);
            }
        }

    }

    private void startApiFormJobWithInput(String path) throws IOException {
        if(path != null) throw new UnsupportedOperationException("Api Form input not implemented yet!");
        String apiUrl = ((String) job.getInput("url"));
        String formPath = ((String) job.getInput("form"));

        //provided format: "ranger":"1-3"
        String range = (String) ((Map) job.getInput("provided")).get("range");
        int from = Integer.parseInt(range.split("-")[0]);
        int to = Integer.parseInt(range.split("-")[1]);

        //read form
        String form = StringUtil.readBody(new File(formPath));

        for (int page = from; page <= to; page++) {
            String scrapeForm = form.replace("{range}",String.valueOf(page));
            List<NameValuePair> pairList = StringUtil.convertToPairs(scrapeForm);

            //name -> match Map
            Map<String, String> groupedRegex = new HashMap<>();
            groupedRegex.put("range", String.valueOf(page));

            forwardApiFormOutput(apiUrl, pairList, groupedRegex);
        }

    }

    private void startHtmlJobWithInput(String path) throws IOException {
        //get base url
        String baseUrl = ((String) job.getInput("url"));
        String type = (String) job.getInput("type");

        //input file: parse it, create for each parse match an output
        if(path != null){
            // read input file into string
            File inputFile = new File(path);
            String body = StringUtil.readBody(inputFile);

            // parse files and create regex groups
            Map<String, Object> parser = (Map<String, Object>) job.getInput("parse");
            String regex = (String) parser.get("regex");
            Map<String,Integer> groups = (Map<String, Integer>) parser.get("groups");

            //match regex with input file
            Pattern p = Pattern.compile(regex);
            Matcher m = p.matcher(body);
            while (m.find()) {
                //name -> match Map
                Map<String, String> groupedRegex = new HashMap<>();

                //parse specified groups into map (name -> match) for later retrieval
                groups.forEach((name, group) -> groupedRegex.put(name, m.group(group)));

                //generate urls
                { // find {} groups
                    String scrapeUrl = StringUtil.replaceArguments(baseUrl, groupedRegex);

                    //save and forward output, if specified
                    if(type.equals("IMAGE"))
                        forwardImageOutput(scrapeUrl,groupedRegex);
                    else
                        forwardHtmlOutput(scrapeUrl, groupedRegex);
                }
            }
        }
        //no input file: paginated url scraping
        //expect 'provided' map with 'range' key and int range values separated by '-'
        else{
            //provided format: "ranger":"1-3"
            String range = (String) ((Map) job.getInput("provided")).get("range");
            int from = Integer.parseInt(range.split("-")[0]);
            int to = Integer.parseInt(range.split("-")[1]);

            for (int page = from; page <= to; page++) {
                String scrapeUrl = baseUrl.replace("{range}",String.valueOf(page));

                //name -> match Map
                Map<String, String> groupedRegex = new HashMap<>();
                groupedRegex.put("range", String.valueOf(page));

                forwardHtmlOutput(scrapeUrl, groupedRegex);
            }
        }
    }

    private void forwardApiOutput(String apiUrl, String requestBody, Map<String, String> groupedRegex) {
        //check if already downloaded
        String savePath = ((String) job.getOutput("path"));
        savePath = StringUtil.replaceArguments(savePath, groupedRegex);

        String apiMessage = (String) job.getInput("api-message");
        apiMessage = StringUtil.replaceArguments(apiMessage, groupedRegex);

        if(new File(savePath).exists() && !job.force){
            log.debug("[{}] Skipping api requests, {} already exists", job.id, savePath);
            if(job.forward != -1)
                jobs.getJob(job.forward).forwardedStart(savePath);
        } else{
            queueApiRequest(apiUrl, requestBody, savePath, apiMessage);
        }
    }

    private void forwardApiFormOutput(String apiUrl, List<NameValuePair> pairList, Map<String, String> groupedRegex) {
        //check if already downloaded
        String savePath = ((String) job.getOutput("path"));
        savePath = StringUtil.replaceArguments(savePath, groupedRegex);

        String apiMessage = (String) job.getInput("api-message");
        apiMessage = StringUtil.replaceArguments(apiMessage, groupedRegex);

        if(new File(savePath).exists() && !job.force){
            log.debug("[{}] Skipping api requests, {} already exists", job.id, savePath);
            if(job.forward != -1)
                jobs.getJob(job.forward).forwardedStart(savePath);
        } else{
            queueApiFormRequest(apiUrl, pairList, savePath, apiMessage);
        }
    }

    private void forwardHtmlOutput(String scrapeUrl, Map<String, String> groupedRegex) {
        //check if already downloaded
        String savePath = ((String) job.getOutput("path"));
        savePath = StringUtil.replaceArguments(savePath, groupedRegex);

        if(new File(savePath).exists() && !job.force){
            log.debug("[{}] Skipping {}, already exists", job.id, scrapeUrl);
            if(job.forward != -1)
                jobs.getJob(job.forward).forwardedStart(savePath);
        } else{
            queueHtmlUrl(scrapeUrl, savePath);
        }
    }

    private void forwardImageOutput(String scrapeUrl, Map<String, String> groupedRegex) {
        //check if already downloaded
        String savePath = ((String) job.getOutput("path"));
        savePath = StringUtil.replaceArguments(savePath, groupedRegex);

        if(new File(savePath).exists() && !job.force){
            log.debug("[{}] Skipping {}, already exists", job.id, scrapeUrl);
            if(job.forward != -1)
                jobs.getJob(job.forward).forwardedStart(savePath);
        } else{
            queueImageUrl(scrapeUrl, savePath);
        }
    }

    private void queueImageUrl(String scrapeUrl, String savePath) {
        log.trace("[{}] Queuing {}",job.id, scrapeUrl);
        if(job.forward != -1) {
            jobCalls.scrapeImageAndForward(scrapeUrl, savePath,
                    job.proxies, job.release, jobs.getJob(job.forward)::forwardedStart, (String) job.getOutput("type"));
        } else{
            jobCalls.scrapeImage(scrapeUrl, savePath,
                    job.proxies, job.release, (String) job.getOutput("type"));
        }
    }

    private void queueHtmlUrl(String scrapeUrl, String savePath) {
        log.trace("[{}] Queuing {}",job.id, scrapeUrl);
        if(job.forward != -1) {
            jobCalls.scrapeHtmlAndForward(scrapeUrl, savePath,
                    job.proxies, job.release, jobs.getJob(job.forward)::forwardedStart, (String) job.getOutput("type"));
        } else{
            jobCalls.scrapeHtml(scrapeUrl, savePath,
                    job.proxies, job.release, (String) job.getOutput("type"));
        }
    }

    private void queueApiRequest(String apiUrl, String requestBody, String savePath, String apiMessage) {
        log.trace("[{}] Queuing API {}",job.id, requestBody);
        if(job.forward != -1) {
            jobCalls.scrapeApiAndForward(apiUrl, requestBody, savePath,
                    job.proxies, job.release, jobs.getJob(job.forward)::forwardedStart,
                    (String) job.getOutput("type"), apiMessage);
        } else{
            jobCalls.scrapeApi(apiUrl, requestBody, savePath,
                    job.proxies, job.release, (String) job.getOutput("type"), apiMessage);
        }
    }

    private void queueApiFormRequest(String apiUrl, List<NameValuePair> pairList, String savePath, String apiMessage) {
        log.trace("[{}] Queuing API Form {}",job.id, pairList.size());
        if(job.forward != -1) {
            jobCalls.scrapeApiFormAndForward(apiUrl, pairList, savePath,
                    job.proxies, job.release, jobs.getJob(job.forward)::forwardedStart,
                    (String) job.getOutput("type"), apiMessage);
        } else{
            jobCalls.scrapeApiForm(apiUrl, pairList, savePath,
                    job.proxies, job.release, (String) job.getOutput("type"), apiMessage);
        }
    }

    public JobPojo getJob() {
        return job;
    }
}
