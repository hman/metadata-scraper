package com.pettt.hman.metadata.scraper.config;

import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;

@Service
public class Executors {
    private Map<String, Executor> executorsMap = new ConcurrentHashMap<>();

    public void execute(String group, Runnable task){
        Executor pool = executorsMap.get(group);
        if(pool == null) {
            pool = createExecutor(group);
        }

        pool.execute(task);
    }

    private Executor createExecutor(String group) {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(200);
        executor.setMaxPoolSize(200); //200 pages at once
        executor.setQueueCapacity(Integer.MAX_VALUE);
        executor.setThreadNamePrefix(group);
        executor.initialize();

        executorsMap.put(group, executor);

        return executor;
    }
}
