package com.pettt.hman.metadata.scraper;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pettt.hman.metadata.scraper.async.AsyncJobCalls;
import com.pettt.hman.metadata.scraper.http.ProxyReservation;
import com.pettt.hman.metadata.scraper.jobs.JobPojo;
import com.pettt.hman.metadata.scraper.jobs.ScrapeJob;
import com.pettt.hman.metadata.scraper.util.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

import java.io.File;
import java.io.IOException;
import java.util.*;

@SuppressWarnings("SpringAutowiredFieldsWarningInspection")
@SpringBootApplication
@EnableAsync
@Slf4j
public class MetadataScraperApplication implements CommandLineRunner{
	private String[] args;
	private Map<Integer,ScrapeJob> scrapeJobs = new HashMap<>();

	private static ObjectMapper objectMapper = new ObjectMapper();

	@Autowired AsyncJobCalls jobCalls;
	@Autowired ProxyReservation proxyReservation;

	public static void main(String[] args) {
		SpringApplication.run(MetadataScraperApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		this.args = args;

		//read scrape arguments
		log.info("Parsing scrape jobs");
		parseJobs();

		//insert proxies
		Set<String> proxyGroups = new HashSet<>();
		scrapeJobs.forEach((id, job) -> proxyGroups.add(job.getJob().proxies));
		String proxyPath = StringUtil.getArgument(args, "proxies:");
		proxyReservation.insertProxies(proxyPath, proxyGroups);

		//validate dependencies
		log.info("Validating dependencies between scrape jobs");
		validateJobs();

		//start jobs
		log.info("Starting scrape jobs");
		startScrapeJobs();

		//TODO check progress
		//noinspection InfiniteLoopStatement user should quit process
		while(true){
			Thread.sleep(3000 * 1000);
			proxyReservation.resetBadProxies(proxyGroups);
		}
	}

	private void startScrapeJobs() {
		scrapeJobs.forEach((id,job) -> {
			log.info("Starting job@{} '{}'",id, job.getJob().name);
			try {
				job.start();
			} catch (Exception e) {
				log.error("Failed job@{} '{}'",id, job.getJob().name);
				e.printStackTrace();
			}
		});
	}

	private void validateJobs() {
		log.warn("Validation not implemented");
	}

	private void parseJobs() throws IOException {
		//get paths for each scrape job
		List<String> paths = StringUtil.getAllArguments(args,"scrape:");

		//try to convert into a object via mapper
		for (String path : paths) {
			convertScrapeJob(path);
		}

		List<String> folderPath = StringUtil.getAllArguments(args, "scrapeFolder:");
		for (String folders: folderPath) {
			File folder = new File(folders);
			//noinspection ConstantConditions
			for (File file : folder.listFiles()) {
				convertScrapeJob(file.getPath());
			}
		}

		log.info("Parsed {} scrape jobs", scrapeJobs.size());
	}

	private void convertScrapeJob(String path) throws IOException {
		if(!path.endsWith(".scrape")) return;
		log.info("Parsing {}",path);
		JobPojo job = objectMapper.readValue(new File(path), JobPojo.class);
		ScrapeJob jobBean = new ScrapeJob(jobCalls, this, job);
		scrapeJobs.put(job.id,jobBean);
	}

	public ScrapeJob getJob(int forward) {
		return scrapeJobs.get(forward);
	}
}
